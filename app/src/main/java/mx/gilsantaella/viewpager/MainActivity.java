package mx.gilsantaella.viewpager;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.btnDialogo);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        CustomDialogFragment dialog = new CustomDialogFragment();
        dialog.show(getFragmentManager().beginTransaction(), "txn_tag");
    }
}

